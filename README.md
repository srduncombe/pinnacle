# Pinnacle Report

Visualizing gene expression in a circular diagram. Works in a browser or from the command line.

## Installation

Install NodeJS version > 4 and the NPM. On an OSX device your best bet is to use Homebrew:

````
brew install node
````

Install the node dependencies with:

````
npm install
````

## Development

To launch a development environment run:

````
npm start
````

Which will:

 * Launch `watchify` to compile the javascript libraries into a single source `app/pinnacle.js`
 * Launch a [LiveReload](http://livereload.com/) server
 * Start a static server at [http://localhost:8080](http://localhost:8080) with `/app` as the root direct.

## Running

### To create a report from the command line:

````
node report.js app/config.json app/genes.csv app/fusions.csv app/expressions.csv path_to_pdf.pdf
````

### To create a visualization from the command line:

````
node visualize.js app/example.csv path_to_svg.svg
````
