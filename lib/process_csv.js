function CSVtoArray(text) {
  text = text.replace(/\“|\”/g, '"')

  var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
  var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;

  // Return if input string is not well formed CSV string.
  if(!re_valid.test(text)) return

  var values = []

  text.replace(re_value, // "Walk" the string using replace with callback.
      function(m0, m1, m2, m3) {
          if(m1 !== undefined) values.push(m1.replace(/\\'/g, "'")) // Remove backslash from \' in single quoted values.
          else if(m2 !== undefined) values.push(m2.replace(/\\"/g, '"')) // Remove backslash from \" in double quoted values.
          else if(m3 !== undefined) values.push(m3)
          return '' // Return empty string.
      })

  // Handle special case of empty last value.
  if (/,\s*$/.test(text)) values.push('')

  return values
}

function ProcessCSV(raw_csv, callback, skip_first) {
  var raw_csv = raw_csv.split(/\r\n|\r|\n/g),
      parsed = [],
      skip_first = typeof skip_first !== 'undefined' ? skip_first : true

  if( skip_first ) raw_csv.shift()

  for (var i = 0; i < raw_csv.length; i++) {
    var raw = raw_csv[i]
    if(raw === '') return
    callback.call(this, CSVtoArray(raw), i)
  }
}

module.exports = ProcessCSV
