function GeneTable(results, document) {
  var tbodies = document.querySelectorAll('.genes tbody'),
      per_table = 52,
      gene = 0

  for (var t = 0; t < tbodies.length; t++) {
    var tbody = tbodies[t]

    for (var i = 0; i < per_table; i++) {
      var tr = document.createElement('tr'),
          data = results.data[results.keys[gene]]

      if( typeof data === 'undefined' ) return

      if( data.is_highlight ) {
        tr.className = 'outlier z_score__'+results.outliers()[gene]
      }

      var cols = ['gene', 'tpm', 'z_score', 'fold_change']

      for (var k = 0; k < cols.length; k++) {
        var td = document.createElement('td'),
            value = data[cols[k]]
            float = parseFloat(value)

        if( !isNaN(float) ) value = float.toFixed(2)
        if( k === 0 ) td.classList.add('gene')
        td.textContent = value

        tr.appendChild(td)
      }

      tbody.appendChild(tr)
      gene += 1

    }
  }
}

module.exports = GeneTable
