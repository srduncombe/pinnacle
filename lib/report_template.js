function ReportTemplate(config, document) {
  var template_objs = document.querySelectorAll('[data-template]'),
      defaults = {
        "date_reported": (new Date).toLocaleDateString()
      }

  for (var i = template_objs.length - 1; i >= 0; i--) {
    var key = template_objs[i].getAttribute('data-template')
    template_objs[i].textContent = config[key] || defaults[key]
  }
}

module.exports = ReportTemplate
