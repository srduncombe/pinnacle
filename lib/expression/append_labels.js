function appendLabels(d3) {
  var config = this.config,
      svg = this.svg,
      genes = this.expression,
      x_scale = this.x_scale,
      x_padding = this.x_padding,
      y_scale = this.y_scale,
      y_zero = this.bottom

  var x_pos = function(num) {
    return x_scale(num)+x_padding
  }

  var labels = svg.append("svg:g")
                  .attr("class", "x_labels")
                  .selectAll('g')
                  .data(genes)
                  .enter()
                  .append("svg:g")
                  .attr("transform", function(el, i) {
                    return "translate("+[x_pos(i), y_zero].join(",")+")" })

  labels.append('svg:circle').attr('r', 13)

  labels.append("svg:text")
        .attr("transform", function(el) { return "translate("+(el.gene.length * -1.75)+", 2)" })
        .text(function(el) { return el.gene })

  var z_scores_offset = config.height + config.offset_y * 1.8

  svg.append("svg:g")
     .attr("class", "z_scores")
     .selectAll('text')
     .data(genes)
     .enter()
     .append("svg:text")
     .text(function(el) { return el.z_score.toFixed(1) })
     .attr("transform", function(el, i) {
       return "translate("+[x_pos(i) - 4, z_scores_offset].join(",")+")" })

  svg.select('g.z_scores')
     .append("svg:text")
     .attr("class", "label")
     .text('Z Score =')
     .attr("transform", function(el, i) {
        return "translate("+[config.offset_x /4, z_scores_offset].join(",")+")" })

  svg.append("svg:g")
     .attr("class", "y_labels")
     .selectAll('g')
     .data(this.y_axes)
     .enter()
     .append("svg:g")
     .attr("transform", function(el, i) {
        return "translate("+[config.width - config.offset_x / 1.1, y_scale(el) + 2].join(",")+")" })
     .append("svg:text")
     .text(function(el) { return el.toString().replace('0.','.') })

  svg.select("g.y_labels")
     .append("svg:text")
     .attr("class", "label")
     .text('TPM')
     .attr("transform", function(el, i) {
        return ["translate("+[config.width - config.offset_x /2.5, config.height/3].join(",")+")",
                "rotate(90, 0 0)"].join(' ') })

}

module.exports = appendLabels
