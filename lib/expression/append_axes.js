function appendAxes(d3) {
  var config = this.config,
      svg = this.svg,
      y_scale = this.y_scale

  var xAxis = d3.svg.axis()
                    .scale(this.x_scale)

  // X Axes
  svg.append("svg:g")
     .attr("class", "x_axes")
     .selectAll("g")
     .data(this.y_axes)
     .enter()
     .append("svg:line")
     .attr("class", function(el) { return "axis axis__"+el })
     .attr("x1", config.offset_x)
     .attr("y1", function(el) { return y_scale(el) })
     .attr("x2", config.width - config.offset_x)
     .attr("y2", function(el) { return y_scale(el) })

  svg.select("g.x_axes")
     .append("svg:line")
     .attr("class", "base")
     .attr("x1", config.offset_x/4)
     .attr("y1", y_scale(0.001))
     .attr("x2", config.width - config.offset_x/4)
     .attr("y2", y_scale(0.001))

  svg.select("g.x_axes")
     .append("svg:line")
     .attr("class", "zero")
     .attr("x1", config.offset_x/1.5)
     .attr("y1", y_scale(0.001))
     .attr("x2", config.width - config.offset_x/1.5)
     .attr("y2", y_scale(0.001))
}

module.exports = appendAxes
