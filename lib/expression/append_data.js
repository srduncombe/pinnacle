
function appendData(d3) {
  var config = this.config,
      expression = this.expression,
      svg = this.svg,
      x_scale = this.x_scale,
      x_padding = this.x_padding,
      y_scale = this.y_scale,
      groups = expression[0].groups.length,
      bottom = this.bottom,
      graph_gap = 7

  var x_pos = function(gene_group, num) {
    if( groups == 2 ) return [graph_gap * -0.5, graph_gap * 0.5][num]
    if( groups == 3 ) return [graph_gap * -1, 0, graph_gap][num]
    return [graph_gap * -1.5,graph_gap * -0.5, graph_gap * 0.5, graph_gap * 1.5][num]
  }

  svg.append("svg:g")
     .attr("class", "points")
     .selectAll("g")
     .data(expression)
     .enter()
     .append("svg:g")
     .attr("transform", function(el, i) {
        var x_pos = x_scale(i) + x_padding
        return "translate("+[x_pos, bottom].join(",")+")" })
     .selectAll("line.graph")
     .data(function(gene) { return gene.groups })
     .enter()
     .append("svg:line")
     .attr("class", "graph")
     .attr("x1", x_pos)
     .attr("y1", 0)
     .attr("x2", x_pos)
     .attr("y2", function(exp) { return y_scale(exp) - bottom })
     .style("stroke", function(_, num) { return ['#0A638A', '#F37920'][num] })

}

module.exports = appendData
