function makeData(genes, deviants) {
  var genes = genes || 318,
      deviants = deviants || genes * 0.025,
      data = []

  function fakeGeneName(size) {
    var size = size || 2,
        text = "",
        possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < size; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  function fakeGeneExpression(chance) {
    var seed = Math.random()
    if( seed > chance ) return 0
    return Math.ceil((seed / chance) * 30 - 15)
  }

  for (var i = 0; i < genes; i++) {
    data.push([fakeGeneName(),'',fakeGeneExpression(deviants/genes)].join(','))
  }

  return new Result(data.join("\n"))
}

module.exports = makeData
