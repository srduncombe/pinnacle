var d3 = require('d3'),
    blobUtil = require('blob-util')

function toPNG(height, width, svg, callback) {
  var doctype = ['<?xml version="1.0" standalone="no"?>',
                 '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" ',
                 '"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">'].join(''),
      source = (new XMLSerializer()).serializeToString(svg.node()),
      blob = new Blob([doctype + source],
                      { type: 'image/svg+xml;charset=utf-8' }),
      url = window.URL.createObjectURL(blob),
      img = d3.select('body')
              .append('img')
              // .attr('style','display:none')
              .attr('width', width)
              .attr('height', height)
              .node(),
      callback = callback || function() { }

  img.onload = function() {
    var canvas = d3.select('body')
                   .append('canvas')
                   .attr('style','display:none')
                   .attr('width', width)
                   .attr('height', height)
                   .node()

    var ctx = canvas.getContext('2d')
    ctx.drawImage(img, 0, 0);

    callback(canvas.toDataURL("image/png"))
    // canvas.remove()
    // img.remove()
  }

  img.src = url
}

module.exports = toPNG
