function List(document) {
  var element = document.querySelector('#immune_list')

  for(var category in this.immune) {
    var heading = document.createElement('h4'),
        ul  = document.createElement('ul'),
        list = this.immune[category],
        className = category.replace(/\s/g, '_').replace(/\W/g, '').toLowerCase()

    ul.className = className
    heading.className = className
    heading.textContent = category

    for(var item in list) {
      var li = document.createElement('li'),
          name_span = document.createElement('span'),
          percentage_span = document.createElement('span'),
          indicator = list[item]

      name_span.textContent = indicator.name
      percentage_span.textContent = indicator.percentage

      li.appendChild(name_span)
      li.appendChild(percentage_span)
      ul.appendChild(li)
    }

    element.appendChild(heading)
    element.appendChild(ul)
  }

}

module.exports = List
