function Circle(d3) {
  var radius = 360,
      labels = [],
      sections = [],
      sum = 0

  for (var i = 0; i < this.dist.length; i++) {
    labels.push(this.dist[i].type)
    sections.push(this.dist[i].percentage)
    sum += this.dist[i].percentage
  }

  var offset = 0.25 + sections[0]/sum/2

  var stepAngle = function(step, include_step, include_half_step) {
    var total = 0
    if( include_step ) step += 1
    for(var i = 0; i < step; i++) {
      total += include_half_step && i === step - 1 ? sections[i]/2 : sections[i]
    }
    return (total / sum  - offset) * 2 * Math.PI
  }

  var arc = d3.svg
              .arc()
              .startAngle(function(step) { return stepAngle(step) })
              .endAngle(function(step) { return stepAngle(step, true) })
              .innerRadius(function() { return 0 })
              .outerRadius(function(step, index) { return radius / 2 })

  var svg = d3.select('#immune_graph')
              .append('svg')
              .style('height', radius + 30 +'px')
              .style('width', radius + 20 + 'px')

  var slices = svg.append("svg:g")
                  .attr("class", "circles")
                  .attr("transform", "translate(" + (radius/2 + 6) + "," + (radius/2 + 14) + ")")
                  .selectAll("path")
                  .data(d3.range(0, 3, 1))
                  .enter()

  slices.append("svg:path")
        .attr('class', function(step) { return labels[step].toLowerCase() })
        .attr("d", arc)

  slices.append("svg:circle")
        .attr('r', radius/6 +'px')
        .style('fill', 'white')

  var textStep = function(step) {
    var offset = step === 0 ? 80 : 90
    return stepAngle(step, true, true)/Math.PI * 180 - offset
  }

  var texts = slices.append("svg:g")
                    .attr("class", function(step) { return labels[step].toLowerCase() })
                    .attr("transform", function(step) {
                      return "rotate("+textStep(step)+") translate("+radius/3+")"
                    })
  var radius = 36

  texts.append("svg:circle")
       .attr("class", "container")
       .attr("r", radius)

  texts.append("svg:text")
       .attr("transform", function(step) {
          var offset = [radius / -2 , radius / 2 ]
          return "rotate("+-textStep(step)+") translate("+offset.join(' ')+")" })
       .text(function(step) { return sections[step] * 100 +'%' })

  texts.append("svg:text")
       .attr('class', 'label')
       .attr("transform", function(step) {
          var rotation = "rotate("+-textStep(step, 90)+")",
              width = labels[step].length * (step > 0 ? 7 : 12) / -2,
              offset = [width, radius / -8 ]

          return [rotation, "translate("+offset.join(' ')+")"].join(" ") })
       .text(function(step) { return labels[step] })
}

module.exports = Circle
