var ProcessCSV = require('./process_csv')

function Expression(expression, d3) {
  var config = { width: 610,
                 height: 74,
                 offset_y: 8,
                 offset_x: 40 }

  this.expression = []
  this.config = config

  ProcessCSV.call(this, expression, function(row) {
    this.expression.push({
      gene: row[0],
      z_score: parseFloat(row[1]),
      groups: [parseFloat(row[2]), parseFloat(row[3])]
    })
  })

  this.svg = d3.select('#expression')
               .append("svg:svg")
               .attr("width", this.config.width + 'px')
               .attr("height", this.config.height + 18 + 'px')

  this.x_scale =  d3.scale.ordinal()
                          .domain(d3.range(this.expression.length))
                          .rangeRoundBands([0, this.config.width - this.config.offset_x * 2])
  this.x_padding = config.offset_x + (this.x_scale(2) - this.x_scale(1))/2

  this.y_scale = function(expression) {
    var scale = d3.scale
                  .log()
                  .domain([0.001, 100])
                  .range([0, config.height - config.offset_y * 2])

    return config.height - scale(expression) - config.offset_y
  }
  this.bottom = this.y_scale(0.001)
  this.y_axes = [0.01, 0.1, 1, 10, 100]

  require('./expression/append_axes').call(this, d3)
  require('./expression/append_data').call(this, d3)
  require('./expression/append_labels').call(this, d3)

  return this
}

module.exports = Expression
