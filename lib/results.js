var ProcessCSV = require('./process_csv')

function Result(data, steps) {
  var steps = steps || 5

  this.gene = data[0],
  this.tpm = data[1],
  this.fold_change = data[3],
  this.z_score = parseFloat(data[2]),
  this.value = Math.round(this.z_score/steps)

  if( isNaN(this.value) ) return
  if( this.value > 3 ) this.value = 3
  if( this.value > 3 ) this.value = 3
  if( this.value === 0 ) this.value = 1

  return this
}

function Results(data, highlights, config) {
  var config = config || {},
      z_score_limit = config.z_score_limit || 5,
      steps = config.steps || 5

  this.data = {}
  this.keys = []
  this.values = []

  this.highlights = { data: {}, keys: [] }

  ProcessCSV.call(this, highlights, function(line) {
    var result = new Result(line, steps)
    if( !result ) return
    this.highlights.data[result.gene] = result
    this.highlights.keys.push(result.gene)
  })

  var highlight_data = this.highlights.data
  this.highlights.keys.sort(function(gene_a_key, gene_b_key) {
    var gene_a = highlight_data[gene_a_key],
        gene_b = highlight_data[gene_b_key]
    return gene_a.z_score > gene_b.z_score ? -1 : 1
  })

  ProcessCSV.call(this, data, function(line) {
    var result = new Result(line, steps)
    if( !result ) return
    result.is_highlight = typeof this.highlights.data[result.gene] !== 'undefined'
    this.data[result.gene] = result
    this.keys.push(result.gene)
    this.values.push(result.value)
  })

  this.length = this.keys.length

  var outliers = null

  this.outliers = function() {
    if( !!outliers ) return outliers

    outliers = []

    for (var i = 0; i < this.length; i++) {
      var value = this.values[i],
          gene = this.keys[i]

      if( gene.is_highlight ) outliers.push(value)
      else outliers.push(0)
    }
    return outliers
  }
}

module.exports = Results
