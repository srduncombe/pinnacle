var ProcessCSV = require('./process_csv')

function Immune(immune_list, distribution, document, d3) {
  this.immune = {}
  this.dist = distribution

  ProcessCSV.call(this, immune_list, function(row) {
    var name = row[0],
        type = row[3],
        cat = row[1],
        percent = parseFloat(row[2])

    this.immune[cat] = this.immune[cat] || []
    this.immune[cat].push({ name: name, percentage: percent })
  })

  require('./immune/list').call(this, document)
  require('./immune/circle').call(this, d3)
}

module.exports = Immune
