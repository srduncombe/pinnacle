# Paragon Report

## Version 1.1
**Commit**: [f298dda](https://cofactorgenomics.beanstalkapp.com/paragon-report/changesets/f298dda) | **Screenshot**: [version_1.1.png](https://cofactorgenomics.beanstalkapp.com/paragon-report/browse/git/changes/version_1.1.png) | **Date:** *January 16th, 2016*

  * Moved circle graph labels into the graph
  * Updated the gradient in the header to better match design
  * Fix Naïve / Dendritic misspelling

## Version 1
**Commit**: [c4217c3](https://cofactorgenomics.beanstalkapp.com/paragon-report/changesets/c4217c3) | **Screenshot**: [version_1.png](https://cofactorgenomics.beanstalkapp.com/paragon-report/browse/git/changes/version_1.png) | **Date:** *December 2nd, 2016*

  * Redesigned to include expresssion graph, immune deconvolution, mutational burden
  * Removed circular pinnacle visualization

## Version 0
**Commit**: [9e618ea](https://cofactorgenomics.beanstalkapp.com/paragon-report/changesets/9e618ea) | **Screenshot**: [version_0.png](https://cofactorgenomics.beanstalkapp.com/paragon-report/browse/git/changes/version_0.png) | **Date:** *June 2nd, 2016*

  * Based off of Pinnacle Report
  * Included modified circular visualization
