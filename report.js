var fs = require('fs'),
    pdf = require('html-pdf')
    jsdom = require('jsdom'),
    argv = require('yargs')
           .demand(3)
           .usage('Usage: $0 config.json immune.csv expression.csv')
           .boolean('fail')
           .boolean('verbose')
           .argv

var Paragon = require('./paragon'),
    args = { fail: argv.fail },
    html = fs.readFileSync(__dirname + '/app/report.html', 'utf8')

for (var i = 0; i < Paragon.inputs.length; i++) {
  var filename = argv._[i],
      input = Paragon.inputs[i]

  args[input] = fs.readFileSync(argv._[i], 'utf8')
  if( filename.search('.json') !== -1 ) args[input] = JSON.parse(args[input])
}

var pdf_options = {
  format: 'Letter',
  border: '0',
  base: 'file://'+__dirname + '/app/'
}

var report_file = argv._[4] || './report.pdf'

jsdom.env({
  html: html,
  scripts: ['file://'+__dirname + '/node_modules/d3/d3.min.js'],
  done: function (err, window) {
    var document = window.document

    new Paragon(document, window.d3, args)

    pdf.create(document.documentElement.outerHTML, pdf_options)
       .toFile(report_file, function(err, res) {
          if (err) return console.error(err)
          if( argv.verbose ) console.log(res['filename'])
       });
  }
})


