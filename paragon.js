var d3 = require('d3'),
    ReportTemplate = require('./lib/report_template'),
    Immune = require('./lib/immune'),
    Expression = require('./lib/expression')

function Paragon(document, d3, args) {
  var fail = fail || false,
      config = args.config

  new Expression(args.expression, d3)
  new Immune(args.immune, config.distribution, document, d3)
  new ReportTemplate(config, document)
  document.querySelector('#burden .slider').style.left = (config.burden / 2000 * 100) +'%'
}

Paragon.inputs = ['config', 'immune', 'expression']

Paragon.xhrInit = function(html, files) {
  var data =  false,
      args = {}

  function ready() {
    var is_ready = true

    for(var file in args) is_ready = is_ready && args[file]
    if(!is_ready) return

    args.fail = document.location.hash === '#fail'
    new Paragon(html, d3, args)
  }

  function getFile(file) {
    args[file] = false
    d3.xhr(files[file], function(error, response) {
      var data = response.responseText
      if( files[file].search('.json') !== -1 ) data = JSON.parse(data)
       args[file] = data
      ready()
    })
  }

  for(var file in files) getFile(file)
}

if( typeof window !== 'undefined' ) {
  window.Paragon = Paragon
  window.d3 = d3
} else {
  module.exports = Paragon
}
